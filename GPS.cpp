/*
TinyGPS - a small GPS library for Arduino providing basic NMEA parsing
Based on work by and "distance_to" and "course_to" courtesy of Maarten Lamers.
Suggestion to add satellites(), course_to(), and cardinal(), by Matt Monson.
Precision improvements suggested by Wayne Holder.
Copyright (C) 2008-2013 Mikal Hart
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "GPS.h"


GPS::GPS(){};

//
// public methods
//

bool GPS::encode(char c){
  boolean parsed = false;
  //Serial.print(c);
  switch(c){
    case '$':
      if(s==0) s=1;
      else s=0;
      break;
    case 'G':
      if(s==1) s=2;
      else s=0;
      break;
    case 'P':
      if(s==2) s=3;
      else s=0;
      break;
    case 'R':
      if(s==3) s=4;
      else s=0;
      break;
    case 'M':
      if(s==4) s=5;
      else s=0;
      break;
    case 'C':
      if(s==5){
        s=6;
        datos=0;
      }
      else s=0;
      break;
    case ',':
      if(s==6){
        s=7; k=0;
        for(int i=0;i<12;i++){
          Datos[i]="";
        }
      }
      else if(s>6){
        k++;
      }
      else s=0;
      break;
    case '*':
      if(s==7){
        if(datos > 42 ) parsed = true;
        s=0;
      }
      break;
    default:
      if(s==7){
        datos++;
        Datos[k].concat(c);
      }
      else s=0;
      break;
  }
  return parsed;
}

void GPS::get_position(String *lat, String *lon){
  *lat = Datos[2] + "," + Datos[3];
  *lon = Datos[4] + "," + Datos[5];
}

void GPS::get_Date(uint8_t *day,uint8_t *month,uint8_t *year,uint8_t *hour,uint8_t *minute,uint8_t *second){
  aux = Datos[0].substring(0,2);
  *hour = aux.toInt();
  aux = Datos[0].substring(2,4);
  *minute = aux.toInt();
  aux = Datos[0].substring(4,6);
  *second = aux.toInt();
  aux = Datos[8].substring(0,2);
  *day = aux.toInt();
  aux = Datos[8].substring(2,4);
  *month = aux.toInt();
  aux = Datos[8].substring(4,6);
  *year = aux.toInt();
}

