/*
  Comandos.h - Libreria de Cadenas para comunicacion con Smart One.
  Creada por Jefferson Beleño, Diciembre 3, 2020.
*/
#ifndef Comandos_h
#define Comandos_h
 
#include "Arduino.h"
#include <string.h>
 
class Comandos
{
  public:
    Comandos(int garbage);
    String PromedioNivelGas(float,float,float,float);
    String PromedioNivelGas(float,float,float,float,float);
    String PromedioFlujoGas(float,float,float,float);
    String ConsumoAcumuladoGas(float);
    String DiferenciaGas(float);
    String AlertaEvento(String E,String lat,String lon,String hora);
    String PosDate(String lat,String lon,String fecha,String hora);

    String ObtenerChecksum(String); 
  private:
    String _texto;
    
    uint8_t _checksum;
    byte _msg2[102];
    char longitud;
    String _check;
    uint8_t XORChecksum8(const byte *data, size_t dataLength);
    
};
 
#endif
