# Changelog

- Intercambio de variables independientes por estructuras para Motores y Tanques
- Mejora de eficiencia y legibilidad en funciones asociadas a Motores y Tanques
- Integracion de GPS
- Sincronizacion de Hora GPS con hora local de la teensy cada 6 horas
- Correccion a UTC-5
- Los archivos guardados en SD ahora se llaman "ddmmaa.csv"
- Integracion con codigo SmartOne Crc y Checksum
- Correccion de tipo de dato latitud y longitud

# To Do

- Usar codigo para Enviar por Smart One en el flujo principal
