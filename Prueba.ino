#include <NMEA2000.h>
#include <N2kMessages.h>
#include <NMEA2000_Teensyx.h>
#include <SD.h>
#include <TimeLib.h>
#include "GPS.h"
//Comandos para SmartOne
#include "Comandos.h"

GPS gps; 
#define SerialGPS Serial3
const int offset = -5;  // Bogota GMT-5
uint8_t Year;
uint8_t Month, Day, Hour, Minute, Second;
unsigned long age,fix_age;
String latitud,longitud;

//Variables para trabajo con python y SD
char *nombreFile;
String message = "";
byte datoLecturaSD; //Variable que toma el dato actual
File myFile;//Objeto tipo File que permite acceder y operar la SD

//Objeto NMEA para control y trabajo
tNMEA2000_Teensyx NMEA2000;

//Definicion de Estructura para trabajo con la Nmea 2000
typedef struct {
  unsigned long PGN;
  void (*Handler)(const tN2kMsg &N2kMsg);
} tNMEA2000Handler;

/*//Como se leen dos parametros: flujo de combustible
  //Y Nivel de Tanque, se definen las funciones acordes
  con la libreria N2KMessages*/
void FluidLevel(const tN2kMsg &N2kMsg);
void EngineDynamicParam(const tN2kMsg &N2kMsg);

//En este arreglo debe Incluirse el codigo PGN
//Y la direccion de memoria de la función asociada
tNMEA2000Handler NMEA2000Handlers[] = {
  {127505L, &FluidLevel},
  {127489L, &EngineDynamicParam},
  {0, 0}
};

//Variables Creadas Para Operacion
bool   banderagalones=true;
bool   banderalitros=false;
bool   banderanudos=true;
bool   banderakm=false;
double tiempopromedio = 60000;
double kema = 0.08;

typedef struct {
  double deltaacumulativo=0;
  double deltatm=0;
  double tiempoAnterior=0;
  double tiempoActual=0;
  double medidaflujo=0;
  double consumoinst=0;
  double Acumuladorconsumo=0;
  double consumotiempopromedio=0;
  double deltaacumulativohoras=0;
  double flujomedio=0;
  double tiempoenhoras = 0;
  bool bncompletado = false;
} Motor;

Motor M1;
Motor M2;
Motor M3;

typedef struct {
  String datosPorcentaje[10];
  String datosGalones[10];
  float numerosPorcentaje[10];
  float numerosGalones[10];
  double receptorlv = 0;
  double nivelgalones = 0;
  double ema = 0;
  double prb = 0;
  bool flagema = false;
} Tanque;

Tanque T1;
Tanque T2;
Tanque T3;

//Alarmas
int contador = 0;
byte identificador[3] = {0x30, 0x30, 0x30};

//Variables SmartOne
Comandos comand(1);
String texto;
byte mensaje[55];
uint16_t crcval;
float tanque[4] = {152.1,245.8,278.0,272.8};
float total = 2762.9;
const int handshake_Pin = 18;

//Definicion de funciones desarrolladas
void revisionSerial(void);
void EnvioDeArchivos(void);
void EscrituraSd(void);
void tablas(Tanque *T,int tanque);
void tratamientonivel(Tanque *T);
void clasificacionnivel(Tanque *T);
void CalculoFlujoMotor(Motor *M, double FuelRate, int motor);
void HandleNMEA2000Msg(const tN2kMsg &N2kMsg);
uint16_t crc16(char *pdata, uint16_t length);
void EnviarSmartOne();

time_t ActualizarHora(){
  Serial.println("actualizando hora...");
  if (age < 500) {
    int year = Year+2000;
    setTime(Hour, Minute, Second, Day, Month, year);
    adjustTime(offset * SECS_PER_HOUR);
  }
  return now();
}

void setup() {
  //Inicializacion de Comunicación serial
  //Tanto para computadora como modulo Bluetooth, GPS
  Serial5.begin(9600); //bluetooth
  Serial.begin(115200);
  Serial4.begin(9600); //Smart One
  SerialGPS.begin(9600); //GPS
  pinMode(handshake_Pin, OUTPUT);
  digitalWrite(handshake_Pin , HIGH);
  // Encabezados Necesarios para funcionamiento de la libreria NMEA2000
  NMEA2000.EnableForward(false);
  NMEA2000.SetMsgHandler(HandleNMEA2000Msg);
  NMEA2000.Open();
  // Inicialización de la tarjeta SD
  if (!SD.begin(BUILTIN_SDCARD)){
    Serial.println("No se tiene acceso a la SD card");
    //return;
  }
  Serial.println(" Inicialización realizada.");
  tablas(&T1,1);
  tablas(&T2,2);
  tablas(&T3,3);
  //GPS actualizar hora cada 6 horas
  setSyncProvider(ActualizarHora);
  setSyncInterval(6*SECS_PER_HOUR);
  //Hasta no actualizar Hora no hace nada
  while(timeStatus() == timeNotSet){
    Serial.println("esperando trama gps valida");
    delay(1000);
    if(gps.encode(SerialGPS.read())){
      gps.get_Date(&Day, &Month, &Year, &Hour, &Minute, &Second);
      int year = Year+2000;
      setTime(Hour, Minute, Second, Day, Month, year);
      adjustTime(offset * SECS_PER_HOUR);
      return;
    }
  }
}

//Ciclo de Ejecución Principal
void loop() {
  //Metodo necesario para el funcionamiento adecuado de la libreria NMEA 2000
  NMEA2000.ParseMessages();
  if (Serial5.available()) {
    revisionSerial();
  }
  if ((M1.bncompletado == true) || (M2.bncompletado == true)) {
    EscrituraSd(); //Falta Mandar SmartOne
  }
  //Actualizacion GPS
  if(SerialGPS.available()) {
    if (gps.encode(SerialGPS.read())) { // process gps messages
      // lectura de reloj GPS
      gps.get_Date(&Day, &Month, &Year, &Hour, &Minute, &Second);
    }
    //actualizacion de latitud y longitud
    gps.get_position(&latitud, &longitud);
//    Serial.println(latitud);
//    Serial.println(longitud);
  }
}

//Codificación Funciones
//TratamientoNivel
void clasificacionnivel(Tanque *T) {
  float prom1 = (0 + T->numerosPorcentaje[0]) / 2 ;
  float prom2 = (T->numerosPorcentaje[0] + T->numerosPorcentaje[1]) / 2 ;
  float prom3 = (T->numerosPorcentaje[1] + T->numerosPorcentaje[2]) / 2 ;
  float prom4 = (T->numerosPorcentaje[2] + T->numerosPorcentaje[3]) / 2 ;
  float prom5 = (T->numerosPorcentaje[3] + T->numerosPorcentaje[4]) / 2 ;
  float prom6 = (T->numerosPorcentaje[4] + T->numerosPorcentaje[5]) / 2 ;
  float prom7 = (T->numerosPorcentaje[5] + T->numerosPorcentaje[6]) / 2 ;
  float prom8 = (T->numerosPorcentaje[6] + T->numerosPorcentaje[7]) / 2 ;
  float prom9 = (T->numerosPorcentaje[7] + T->numerosPorcentaje[8]) / 2 ;
  float prom10 = (T->numerosPorcentaje[8] + T->numerosPorcentaje[9]) / 2 ;
  if ((T->receptorlv <= prom1)) {
    T->nivelgalones = 0;
  }
  if ((T->receptorlv > prom1) && (T->receptorlv <= prom2)) {
    T->nivelgalones = T->numerosGalones[0];
  }
  if ((T->receptorlv > prom2) && (T->receptorlv <= prom3)) {
    T->nivelgalones = T->numerosGalones[1];
  }
  if ((T->receptorlv > prom3) && (T->receptorlv <= prom4)) {
    T->nivelgalones = T->numerosGalones[2];
  }
  if ((T->receptorlv > prom4) && (T->receptorlv <= prom5)) {
    T->nivelgalones = T->numerosGalones[3];
  }
  if ((T->receptorlv > prom5) && (T->receptorlv <= prom6)) {
    T->nivelgalones = T->numerosGalones[4];
  }
  if ((T->receptorlv > prom6) && (T->receptorlv <= prom7)) {
    T->nivelgalones = T->numerosGalones[5];
  }
  if ((T->receptorlv > prom7) && (T->receptorlv <= prom8)) {
    T->nivelgalones = T->numerosGalones[6];
  }
  if ((T->receptorlv > prom8) && (T->receptorlv <= prom9)) {
    T->nivelgalones = T->numerosGalones[7];
  }
  if ((T->receptorlv > prom9) && (T->receptorlv <= prom10)) {
    T->nivelgalones = T->numerosGalones[8];
  }
  if ((T->receptorlv > prom10)) {
    T->nivelgalones = T->numerosGalones[9];
  }
}

void tratamientonivel(Tanque *T) {
  if (T->flagema == false) {
    T->ema = (T->nivelgalones * kema) + ((1 - kema) * T->nivelgalones);
    T->flagema = true;
  } else {
    T->prb = ((1 - kema) * T->ema);
    T->ema = (T->nivelgalones * kema) + T->prb;
  }
}
//Funcion Tablas
void tablas(Tanque *T,int tanque) {
  byte datoIn[10];   //Longitud de bytes por renglón leído del archivo
  String datosInString[20];
  int contadorSaltosLinea = 0;
  int contadorBytesLinea = 0;
  int contadorSaltosPrueba = 0;
  int contadorComas = 0;
  char archivo[12];
  sprintf(archivo,"tanque%d.csv",tanque);
  //myFile = SD.open(archivo, FILE_READ);
  message = "tanque"+String(tanque)+".csv";
  char  nombre[message.length()];
  message.toCharArray(nombre, message.length() + 1);
  nombreFile = (char *)nombre;
//  //myFile = SD.open(nombreFile, FILE_READ);
//  myFile = SD.open(nombreFile, FILE_WRITE);
//  //myFile = SD.open(message, FILE_READ);
  if(tanque==1) myFile = SD.open("tanque1.csv", FILE_WRITE);
  if(tanque==2) myFile = SD.open("tanque2.csv", FILE_WRITE);
  if(tanque==3) myFile = SD.open("tanque3.csv", FILE_WRITE);
  if (myFile) {
    Serial.println(archivo);
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      // Serial.print("holi");
      //Lectura Instantanea del dato
      byte lectura = myFile.read();
      //      Serial.println(" ");
      //      Serial.println(" ");
      Serial.write(lectura);
      //Si se encuentra un final de linea(10)
      //Incrementa el indice
      if (lectura == 10) contadorSaltosPrueba ++;
      if ((lectura == 10) && (contadorSaltosLinea == 0)) {
        contadorSaltosLinea++;
        contadorBytesLinea = 0;
        Serial.println("Primera fila ignorada.");
      }
      //Else if que realiza el proceso de Concantenado, y guardado en
      //los arreglos que almacenan de manera individual los datos
      else if ((lectura != 10) && (contadorSaltosLinea >= 1)) {
        if (lectura == 44) {
          switch (contadorComas) {
            case 0:
              T->datosPorcentaje[contadorSaltosLinea - 1] = String((char *)datoIn);
              T->numerosPorcentaje[contadorSaltosLinea - 1] = T->datosPorcentaje[contadorSaltosLinea - 1].toFloat();
              Serial.print("Case 0 Línea String concatenada de la columna ");
              Serial.println(contadorComas + 1);
              Serial.println(T->numerosPorcentaje[contadorSaltosLinea - 1]);
              contadorComas++;
              break;

            default:
              break;
          }
          Serial.println("Salida del switch.");
          //Se limpia el arreglo que
          //almacena datos parciales
          for (int i = 0; i < 10; i++) {
            datoIn[i] = byte(0);
          }
          contadorBytesLinea = 0;
        } else {
          //Control de Indices
          Serial.print("Guardando valores de la línea ");
          Serial.print(contadorSaltosLinea + 1);
          Serial.print(" del archivo en columna ");
          Serial.println(contadorComas + 1);
          //Lectura del archivo
          //Se tiene un arreglo parcial
          //que almacena los datos de los caracteres
          //que contiene cada dato antes de una coma
          datoIn[contadorBytesLinea] = lectura;
          Serial.write(datoIn[contadorBytesLinea]);
          Serial.println(" ");
          contadorBytesLinea++;
        }
      }
      //El concatenado de la ultima columna
      //siempre se hace al final
      else if ((lectura == 10) && (contadorSaltosLinea >= 1)) {
        T->datosGalones[contadorSaltosLinea - 1] = String((char *)datoIn);
        T->numerosGalones[contadorSaltosLinea - 1] = T->datosGalones[contadorSaltosLinea - 1].toFloat();
        Serial.print("Línea String concatenada de la columna ");
        Serial.println(contadorComas + 1);
        Serial.println(T->numerosGalones[contadorSaltosLinea - 1]);
        for (int i = 0; i < 10; i++) {
          datoIn[i] = byte(0);
        }
        Serial.println("Fin de la fila.");
        contadorSaltosLinea++;
        contadorComas = 0;
        contadorBytesLinea = 0;
      }
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(archivo);
  }
  Serial.print("Cantidad de saltos de línea: ");
  Serial.println(contadorSaltosPrueba);
  Serial.println("Fin lectura, archivo cerrado. Datos obtenidos: ");
  Serial.println("Datos columna 1:");
  for (int i = 0; i < 10; i++) {
    Serial.println(T->numerosPorcentaje[i]);
  }
  Serial.println("Datos columna 2:");
  for (int i = 0; i < 10; i++) {
    Serial.println(T->numerosGalones[i]);
  }
  contadorSaltosLinea = 0;
  contadorBytesLinea = 0;
  //contadorIndiceString = 0;
  contadorSaltosPrueba = 0;
  contadorComas = 0;
  for (int i = 0; i < 10; i++) {
    datoIn[i] = byte(0);
  }
}

//Funcion de escritura en la memoria sd
void EscrituraSd() {
  // Creacion o Apertura de un archivo existente
  //message = "241120.csv";
  char archivo[13];
  int dia,mes,ano;
  dia= day(); mes=month(); ano=year()%1000;
  
  //sprintf(archivo,"%02d%02d%d.csv",dia,mes,ano);
  //myFile = SD.open(archivo, FILE_WRITE);
  //myFile = SD.open("121220.csv", FILE_WRITE);
  message = String(dia)+String(mes)+String(ano)+".csv";
  char  nombre[message.length()];
  message.toCharArray(nombre, message.length() + 1);
  nombreFile = (char *)nombre;
  myFile = SD.open(nombreFile, FILE_WRITE);
  if (myFile) {
    myFile.print("hola");
      myFile.print(M1.flujomedio, 4);
      myFile.print(",");
      myFile.print(M1.consumotiempopromedio, 4);
      myFile.print(",");
      myFile.print(M2.flujomedio, 4);
      myFile.print(",");
      myFile.print(M2.consumotiempopromedio, 4);
      //        myFile.print(",");
      //        myFile.print(flujomediom3, 4);
      //        myFile.print(",");
      //        myFile.print(consumotiempopromediom3, 4);
      myFile.print(",");
      myFile.print(T1.ema, 4);
      myFile.print(",");
      myFile.print(T2.ema, 4);
      //        myFile.print(",");
      //        myFile.print(emam3, 4);
      myFile.println();
      //while(myFile.available());
    myFile.close();

    Serial.println("sd funcionando");
    ////////////////////////////////////////////////////////////////////////////////////////
  }else{
    Serial.println("error de guardado en sd");
  }

  
  ///// Funcion del smart one
  //crcval = crc16_lsb(valor, sizeof(valor) / sizeof(valor[0]));


  M1.bncompletado = false;
  M2.bncompletado = false;
  Serial.println("oooooooooooooooooo");
  Serial.println("oooooooooooooooooo");
  Serial.println("oooooooooooooooooo");

}



//Función que revisa lo recibido por el puerto serial
void revisionSerial(void) {
  message = Serial5.readString();
  Serial.println("");
  if (message == 'a') {
    Serial.print("Esto es un Minuto");
    tiempopromedio = 60000;
  }
  else  if (message == 'b') {
    Serial.print("Esto son 2 Minutos");
    tiempopromedio = 120000;
  }

  else  if (message == 'c') {
    Serial.print("Esto son 3 Minutos");
    tiempopromedio = 180000;
  }

  else  if (message == 'd') {
    Serial.print("Esto son 4 Minutos");
    tiempopromedio = 240000;
  }

  else  if (message == 'e') {
    Serial.print("Esto son 5 Minutos");
    tiempopromedio = 300000;
  }
  else  if (message == 'f') {
    Serial.print("Esto son 10 Minutos");
    tiempopromedio = 600000;
  }
  else  if (message == 'g') {
    Serial.print("Esto son 15 Minutos");
    tiempopromedio = 900000;
  }
  else  if (message == 'h') {
    Serial.print("Esto son 20 Minutos");
    tiempopromedio = 1200000;
  }
  else  if (message == 'i') {
    Serial.print("Esto son 25 Minutos");
    tiempopromedio = 1500000;
  }
  else  if (message == 'j') {
    Serial.print("Esto son 30 Minutos");
    tiempopromedio = 1800000;
  }
  else  if (message == 'k') {
    Serial.println("Unidades galones");
    banderagalones=true;
    banderalitros=false;
    Serial.print("Bandera galones: ");
    Serial.println( banderagalones);
    Serial.print("Bandera litros: ");
    Serial.println(banderalitros);
  }
  else  if (message == 'l') {
    Serial.println("Unidades litros");
        banderagalones=false;
    banderalitros=true;
    Serial.print("Bandera galones: ");
    Serial.println( banderagalones);
    Serial.print("Bandera litros: ");
    Serial.println(banderalitros);
  }
  else  if (message == 'm') {
    Serial.println("Unidades Nudos");
        
        banderanudos=true;
    banderakm=false;
    Serial.print("Bandera nd: ");
    Serial.println( banderanudos);
    Serial.print("Bandera km: ");
    Serial.println(banderakm);
  }
  else  if (message == 'n') {
    Serial.println("Unidades Kilometros");
        banderanudos=false;
    banderakm=true;
    Serial.print("Bandera nd: ");
    Serial.println( banderanudos);
    Serial.print("Bandera km: ");
    Serial.println(banderakm);
  }
  else {
    EnvioDeArchivos();
  }
}

//Funcion que Gestiona el envio
//de archivos desde la tarjeta a
//la aplicacion python
void EnvioDeArchivos() {

  //Transicion de la variable String
  //Puntero Char para ejecucion de los metodos sd
  char  nombre[message.length()];
  message.toCharArray(nombre, message.length() + 1);
  nombreFile = (char *)nombre;
  Serial.print(message);

  // Creacion o Apertura de un archivo existente
  myFile = SD.open(nombreFile, FILE_WRITE);
  myFile.close();
  myFile = SD.open(nombreFile, FILE_READ);

  //Mientras el archivo este Disponible
  //Realiza envio de los bytes en su interior
  if (myFile) {
    Serial.println("Contenido de prueba.csv:");

    // Lee mientras hayan datos disponibles (lee byte a byte)
    while (myFile.available()) {
      //Lee el byte recibido desde la SD y lo almacena en la variable tipo byte
      datoLecturaSD = myFile.read();
      //Imprime en consola el dato leído interpretado según ASCII
      //Si se quiere ver el valor real que lee se debe usar print o println
      Serial.write(datoLecturaSD);
      Serial5.write(datoLecturaSD); //Envio Via Bluetooth
    }
    // Cierra el archivo
    myFile.close();
    Serial.println("Cierre del archivo");
    //Arreglo reservado para indicar a la aplicación
    //python que se termino el envio de un archivo
    //.csv
    byte arr[3] = {0x7A, 0xD, 0xA}    ;
    Serial5.write(arr, 3);
    Serial.println("Envie el carac");
  } else {
    // Si no puede abrir el archivo imprime error en monitor serial
    Serial.println("Error abiendo prueba.csv");
  }
}

void CalculoFlujoMotor(Motor *M, double FuelRate, int motor){
  char mensaje[50];
  sprintf(mensaje,"El flujo de combustible en el motor %d es:  ", motor);
  Serial.print(mensaje);
  Serial.println(FuelRate);
  M->tiempoActual = millis();
  M->medidaflujo = FuelRate;
  M->deltatm = M->tiempoActual - M->tiempoAnterior;
  M->tiempoenhoras = M->deltatm / (3600000) ;
  M->consumoinst = M->tiempoenhoras * M->medidaflujo;
  M->Acumuladorconsumo = M->Acumuladorconsumo + M->consumoinst;
  sprintf(mensaje,"El Consumo acumulado en el motor %d es:  ", motor);
  Serial.print(mensaje);
  Serial.println(M->Acumuladorconsumo, 3);
  M->deltaacumulativo = M->deltaacumulativo + M->deltatm;
  Serial.print("El tiempo transcurrido(sg) es: ");
  Serial.println(M->deltaacumulativo / 1000);
  if (M->deltaacumulativo >= tiempopromedio) {
    M->consumotiempopromedio = M->Acumuladorconsumo;
    M->deltaacumulativohoras = M->deltaacumulativo / (3600000);
    M->flujomedio = M->Acumuladorconsumo / M->deltaacumulativohoras;
    Serial.println("---------");
    Serial.print("El flujo medio es: ");
    Serial.println(M->flujomedio, 4);
    Serial.print("El Consumo es: ");
    Serial.println(M->consumotiempopromedio, 4);
    Serial.println("---------");
    M->Acumuladorconsumo = 0;
    M->deltaacumulativo = 0;
    M->bncompletado = true;
  }
  M->tiempoAnterior = M->tiempoActual;
  Serial.println("");
}

void EngineDynamicParam(const tN2kMsg &N2kMsg) {
  unsigned char EngineInstance ;
  double EngineOilPress = 0;
  double EngineOilTemp = 0;
  double EngineCoolantTemp = 0;
  double AltenatorVoltage = 0;
  double  FuelRate = 0;
  double  EngineHours = 0;
  double EngineCoolantPress = 0;
  double EngineFuelPress = 0;
  int8_t  EngineLoad = 0;
  int8_t EngineTorque = 0;
  if (ParseN2kEngineDynamicParam(N2kMsg, EngineInstance, EngineOilPress, EngineOilTemp, EngineCoolantTemp, AltenatorVoltage,
                                 FuelRate, EngineHours, EngineCoolantPress, EngineFuelPress,
                                 EngineLoad, EngineTorque)    ) {
    Serial.print("Valor Instancia flj :  ");
    Serial.println(EngineInstance);
    contador++;
    if (EngineInstance == 0) {
      identificador[0] = 0x31;
      Serial.print("Valor id if inst 0 :  ");
      Serial.println(identificador[0]);
    }
    else if (EngineInstance == 1) {
      identificador[1] = 0x31;
      Serial.print("Valor id if inst 1 :  ");
      Serial.println(identificador[1]);
    }
    else if (EngineInstance == 2) {
      identificador[2] = 0x31;
      Serial.print("Valor id if inst 2 :  ");
      Serial.println(identificador[2]);
    }
    //Tener en cuenta en cuenta el contador
    if (contador == 2) {
      contador = 0;
      for (int i = 0; i < 3; i++) {
        if (identificador[i] == 0x30) {
          /////Alarma, Activar envio del mensaje
          Serial.print("Sensor ");
          Serial.print(i + 1);
          Serial.println(" desconectado");
        }
        if (identificador[i] == 0x31) {
          Serial.print("Sensor ");
          Serial.print(i + 1);
          Serial.println(" conectado");
        }
      }
      for (int i = 0; i < 3; i++) {
        identificador[i] = 0x30;
      }
    }
    Serial.print("Valor id :  ");
    Serial.write(identificador, 3);
    // Selecciona el motor 1
    if (EngineInstance == 0) CalculoFlujoMotor(&M1,FuelRate,1);
    // Selecciona el motor 2
    if (EngineInstance == 1) CalculoFlujoMotor(&M2,FuelRate,2);
    // Selecciona el motor 3
    if (EngineInstance == 2) CalculoFlujoMotor(&M3,FuelRate,3);
  }
}

void FluidLevel(const tN2kMsg &N2kMsg) {
  unsigned char Instance;
  tN2kFluidType FluidType;
  double Level = 0;
  double Capacity = 0;
  if (ParseN2kFluidLevel(N2kMsg, Instance, FluidType, Level, Capacity) ) {
    switch (FluidType) {
      case N2kft_Fuel:
        Serial.print("Nivel de gasolina ");
        break;
      case N2kft_Water:
        Serial.print("Water level: ");
        break;
      case N2kft_GrayWater:
        Serial.print("Gray water level :");
        break;
      case N2kft_LiveWell:
        Serial.print("Live well level :");
        break;
      case N2kft_Oil:
        Serial.print("Oil level :");
        break;
      case N2kft_BlackWater:
        Serial.print("Black water level :");
        break;
    }
    // Selecciona el tanque 1
    if (Instance == 0){
      Serial.println("--------");
      Serial.print("en el tanque 1: ");
      Serial.print(Level); Serial.println("%");
      T1.receptorlv = Level;
      clasificacionnivel(&T1);
      tratamientonivel(&T1);
      Serial.print("los galones son: "); Serial.println(T1.nivelgalones);
      Serial.print("valor ema Tq1: "); Serial.println(T1.ema);
      Serial.println("--------");
    }
    // Selecciona el tanque 2
    if (Instance == 1){
      Serial.println("--------");
      Serial.print("en el tanque 2: ");
      Serial.print(Level); Serial.println("%");
      T2.receptorlv = Level;
      clasificacionnivel(&T2);
      tratamientonivel(&T2);
      Serial.print("los galones son: "); Serial.println(T2.nivelgalones);
      Serial.print("valor ema Tq2: "); Serial.println(T2.ema);
      Serial.println("--------");
    }
    // Selecciona el tanque 3
    if (Instance == 2){
      Serial.println("--------");
      Serial.print("en el tanque 3: ");
      Serial.print(Level); Serial.println("%");
      T3.receptorlv = Level;
      clasificacionnivel(&T3);
      tratamientonivel(&T3);
      Serial.print("los galones son: "); Serial.println(T3.nivelgalones);
      Serial.print("valor ema Tq3: "); Serial.println(T3.ema);
      Serial.println("--------");
    }
  }
}

//NMEA 2000 message handler
void HandleNMEA2000Msg(const tN2kMsg &N2kMsg) {
  int iHandler;
  // Find handler
  for (iHandler = 0; NMEA2000Handlers[iHandler].PGN != 0 && !(N2kMsg.PGN == NMEA2000Handlers[iHandler].PGN); iHandler++);
  if (NMEA2000Handlers[iHandler].PGN != 0) {
    NMEA2000Handlers[iHandler].Handler(N2kMsg);
  }
}

//----------------Smart One-------------------
uint16_t crc16 (char *pData, uint16_t length){
  uint8_t  i,lsb;
  uint16_t  data, crc;    
  crc = 0xFFFF;
  while (length--) {
    data = 0x00FF & (uint8_t) * pData++;
    crc = crc ^ data;
    for (i = 8; i > 0; i--){
      if (crc & 0x0001)
        crc = (crc >> 1) ^ 0x8408;
      else
        crc >>= 1;
    }
  }
  crc = ~crc;
  i = crc >> 8;
  lsb = crc - (lsb << 8);
  crc = (lsb<<8) + i;
  return (crc);
}
void EnviarSmartOne(){
  texto.getBytes(mensaje,56);
  uint8_t lon = mensaje[0]-2;
  //crcval = crc16(mensaje,lon);
  uint8_t i=54;
  while(i!=0){
    i--;
    mensaje[i+1]=mensaje[i];
  }
  mensaje[0]=0xAA;
  mensaje[3]=0x10;
  uint8_t sum1 = mensaje[lon-2];
  uint8_t sum2 = mensaje[lon-1];
  if(sum1>60) mensaje[lon-2]=sum1-0x20;
  if(sum2>60) mensaje[lon-1]=sum2-0x20;
  crcval = crc16(mensaje,lon);
  digitalWrite(handshake_Pin , LOW);
  delay(3);
  //Serial.write(mensaje,lon);
  for(int i=0;i<lon;i++){
    Serial4.print(mensaje[i],HEX);
  }
  Serial4.println(crcval,HEX);
  //recibir respuesta
  digitalWrite(handshake_Pin , HIGH);
}
//--------------------------------------------
